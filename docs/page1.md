# MackDown 實做第一天   


### 前言：
#### 開發環境使用 

-    VScode :  @1.87.2
-    Mkdocs :  @1.5.3 
-    Python :  @3.11.8
-    使用設備為 : MAC

---
## 建制開發環境



-   ### 在vscode中使用mkdocs建立markdown    
       首先需要先下載   [Python](https://www.python.org/downloads/macos/) 以MAC為例 

       ![圖片地址失效](image/python.png)
 
    | Stable Releases | Pre-releases |
    | :--  | :-- |
    | 穩定版發行版(建議安裝) | 還在開發中的版本 | 
    | 通常已經用於生產且經過測試並修復已知的BUG| 可能包含新功能,但也可能包含錯誤或不穩定的代碼|
    

    

    下載完後檢查版本 在終端機輸入   


        python --version     

    ![圖片地址失效](image/pythonv.png)    

    <br>
    **<font color=red>若顯示"command not found: python "  &nbsp;請在python後面加'3'</font>**   

        python3 --version

    ![圖片地址失效](image/python3v.png)


    <br>


 -   ###    安裝MkDocs   
       透過pip安裝MkDocs

        pip install mkdocs       

    ![圖片地址失效](image/pip.png)     

    <br>

       跟檢查python版本時一樣,若顯示command not found請在後面加'3'  
    
        pip3 install mkdocs


       ![圖片地址失效](image/pip3.png)

    <br>

 -   ###    安裝 Material for MkDocs

    採用Material 的樣式與功能   

        pip3 install mkdocs-material     

    <br>

    檢查版本確認安裝成功(因不是python語法所以不用加'3')

        mkdocs --version
    
---

##  新增專案

-   ### 新增專案名稱為       

        mkdocs new my-project  //'my-project' 為自定義檔名


    新增後會生成這樣的檔案
    >my-project     //專案資料夾

    >>docs          //若有圖檔或網頁要有其他分頁需放在此目錄

    >>>index.md     //主畫面

    >>mkdocs.yml    //配置檔

   ![圖片地址失效](image/myproject.png)


-  ### 啟動服務        

        mkdocs serve        

      會看到預設頁面

        

![圖片地址失效](image/mkdserve origin.png)          
     
-     修改mkdocs.yml(配置檔) 
    
             

        site_name: 'Angular Courses 2024' //標題名字
        site_dir: public

        nav:
            - Home: index.md //內頁

        theme:
            name: material //主題樣式

    修改後會如下圖        

![圖片地址失效](image/mkdserve.png)

-   ### 新增子頁        
    使用touch指令在專案中建立子頁面(**<font color=red>需與docs建立在相同目錄下</font>**   )         

        touch docs/home.md       
        touch docs/about.md        
        touch docs/contact.md     

    在mkdocs.yml中新增nav 

        nav:
          - Home: home.md   //Home 為頁面顯示的文字 home.md為路徑
          - About: about.md
          - Contact: contact.md

    ![圖片地址失效](image/setpage.png)      

    <br>

    頁面顯示 Home , About , Contact 路徑
    ![圖片地址失效](image/page.png)    
   
---

##  以上是環境建置概略說明

