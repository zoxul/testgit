### 語法使用注意事項    
-   使用時應注意版面格式及tab用法
-   每段結尾應盡量空白兩格後再按return      

#   基本語法使用

| 語法                             | 效果    | g  |
| :------------------------:      | :-------:   | sa |
|<h1># <br><h2>## <br> <h6>######  | <h1>標題大小 <br><h2>的大小<br><h6>的大小|das|
|                                |                                         |asd|




| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |




| title                   | long title    | title                   |
| :---------------------- | :-----------: | ----------------------: |
| short content           | short content | a long piece of content |
| a long piece of content | short content | short content           |




   `#` 如同Html `<h1>`到`<h6>`

>> 用法開頭為＃
    
```python
    sdadassadas
```

-   #   一個#的大小
-   ######   六個######的大小

```html
<h1>h1的大小</h1>
``` 
<h1>h1的大小</h1> 





-   標點符號用法
    -   `#` 如同Html `<h1>`到`<h6>`


    >> 用法開頭為＃
    
    dsa
    --- 
    sad



    ___     



    adsdsa

    ****

    das




    -   #   一個#的大小
    -   ##  兩個##的大小
    -   ###  三個###的大小
    -   ####   四個####的大小
    -   #####   五個#####的大小
    -   ######   六個######的大小

```html
<h1>h1的大小</h1>
``` 
<h1>h1的大小</h1>




第一天：Markdown 文件格式

習作主題：Markdown 文件格式化和進階元素
內容：
學習 Markdown 基礎，包括標題、粗體、斜體、列表、連結和圖片。
掌握 Markdown 進階元素，如表格、引用、程式碼區塊和腳註。
第二天：MkDocs 環境設置與基礎

習作主題：安裝並設置 MkDocs
內容：
安裝 MkDocs 和必要的依賴。
創建一個 MkDocs 專案並熟悉 mkdocs.yml 配置檔。
第三天：MkDocs 頁面結構與主題

習作主題：建立頁面結構並選擇主題
內容：
在 MkDocs 專案中添加並組織多個頁面。
選擇並套用一個 MkDocs 主題。
第四天：MkDocs 插件與擴展功能

習作主題：使用插件與擴展 Markdown
內容：
研究並安裝至少一個插件，如搜索或社交媒體分享。
使用 Markdown 擴展和添加自訂樣式或腳本。
第五天：結訓展示

習作主題：完成並展示一個完整的 MkDocs 專案
內容：
完成一個有關前端技術的 MkDocs 網站。
展示多頁面、格式化內容、自定義主題和插件功能。
進行網站的簡短演示，展示所學技巧的實際應用。


[OpenAI](https://www.openai.com)

-   ### 如何知道目前有哪些主題可以使用
-   `mkdocs build --help | grep theme`


zoxul@zoxuldeMacBook-Pro my-project %  mkdocs build --help | grep theme   
  -t, --theme [cerulean|cosmo|cyborg|darkly|flatly|journal|litera|lumen|lux|materia|material|minty|mkdocs|pulse|readthedocs|sandstone|simplex|slate|solar|spacelab|superhero|united|yeti]

| Markdown | Html |  
| :-----: | :------: |
| <size=6>我是黑体，绿色，尺寸为5</font> | 文本二   | 
| 文本四 | 文本五   | 
-------------------



| 同样支持修改表格的字体大小   | 只需将表格内容全部放入标签中                                 | 666                   |
| -------- | ---------------------------------- | ---------------------- |
</font>



#   MackDown 實做第二天         

#   MackDown 實做第三天  

#   MackDown 實做第四天  

#   MackDown 實作成果發表  

