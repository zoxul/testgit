#   使用git操作 將專案部署至gitlab

### 建立新專案








#### 指令介紹
>   首先，你需要在你的本地機器上生成一個SSH密鑰。你可以使用以下命令來生成密鑰（如果還沒有的話）：

    ssh-keygen -t rsa -b 4096 -C "your_email@example.com"   //在專案中生成SSH密鑰文件 

    cat ~/.ssh/id_rsa.pub | pbcopy









1.  在你的本地機器上安裝Git（如果還沒有的話）
2.  在GitLab上創建一個新的儲存庫。
3.  在你的本地專案目錄中初始化一個新的Git儲存庫：
    git init
#### 添加你的所有文件到新的Git儲存庫：
    git add

### 提交你的所有文件：
    git commit -m "Initial commit"
### 將你的GitLab儲存庫設定為你的本地儲存庫的遠端：
    git remote add origin git@gitlab.com.username/reoisitory.git
git
請將username/repository.git替換為你的GitLab儲存庫的URL。

將你的變更推送到GitLab：
這將會將你的專案部署到GitLab。